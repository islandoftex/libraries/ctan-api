# The Island of TeX CTAN API

This is a simple JVM library to fetch CTAN's JSON API into Kotlin/Java objects.

## Example

```kotlin
CTAN.authors.firstOrNull { it.userName == "cereda" }?.packages // fetch the packages by Paulo 
```

## License

This application is licensed under the [New BSD License](https://opensource.org/licenses/BSD-3-Clause).
Please note that the New BSD License has been verified as a GPL-compatible free software license by the
[Free Software Foundation](http://www.fsf.org/), and has been vetted as an open source license by the
[Open Source Initiative](http://www.opensource.org/).
