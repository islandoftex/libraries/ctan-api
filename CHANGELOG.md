# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.4.1] - 2024-12-14

### Changed

* Updated dependencies.
* Updated Kotlin to 2.1.0.
* Updated Gradle to 8.10.

## [0.4.0] - 2024-07-15

### Changed

* Distribution now happens as Kotlin Multiplatform Project (KMP). Make sure to update your
  dependencies accordingly (the `-jvm` artifact replaces the  previously published ones).
  (breaking change)
* Switched to `kotlinx.serialization` for serialization instead of Jackson.

### Fixed

* Updated pertinence check in sets to correct reversal (test suite).

## [0.3.0] - 2024-06-30

### Changed

* Updated dependencies.
* Both `CTAN` and `CTANAPI` are not lazy any longer to reflect the needs of API consumers.
  (breaking change)

## [0.2.0] - 2020-08-25

### Added

* Support for CTAN packages following the documented specification (there are open issues).

### Changed

* The `CTAN` object now has `lazy` fields to reflect that they are lazy in the internal `CTANAPI`.

## [0.1.0] - 2020-08-20

### Added

* Initial release.

[Unreleased]: https://gitlab.com/islandoftex/libraries/ctan-api/compare/v0.4.1...master
[0.4.1]: https://gitlab.com/islandoftex/libraries/ctan-api/compare/v0.4.0...v0.4.1
[0.4.0]: https://gitlab.com/islandoftex/libraries/ctan-api/compare/v0.3.0...v0.4.0
[0.3.0]: https://gitlab.com/islandoftex/libraries/ctan-api/compare/v0.2.0...v0.3.0
[0.2.0]: https://gitlab.com/islandoftex/libraries/ctan-api/compare/v0.1.0...v0.2.0
[0.1.0]: https://gitlab.com/islandoftex/libraries/ctan-api/-/tags/v0.1.0
