import com.github.benmanes.gradle.versions.updates.DependencyUpdatesTask
import org.gradle.api.tasks.testing.logging.TestLogEvent

plugins {
    kotlin("multiplatform") version "2.1.0"
    kotlin("plugin.serialization") version "2.1.0"

    id("org.jetbrains.dokka") version "1.9.20"
    id("com.diffplug.spotless") version "6.25.0"
    id("com.diffplug.spotless-changelog") version "3.1.2"
    id("com.github.ben-manes.versions") version "0.51.0"
    id("io.gitlab.arturbosch.detekt") version "1.23.7"

    id("maven-publish")
}

if (!project.hasProperty("JobToken")) {
    logger.warn(
        """
            Island of TeX ----------------------------------------------
            Will be unable to publish (jobToken missing)
            Ignore this warning if you are not running the publish task
            for the GitLab package registry.
            ------------------------------------------------------------
        """.trimIndent()
    )
}

spotlessChangelog {
    changelogFile("CHANGELOG.md")
    setAppendDashSnapshotUnless_dashPrelease(true)
    ifFoundBumpBreaking("breaking change")
    tagPrefix("v")
    commitMessage("Release v{{version}}")
    remote("origin")
    branch("master")
}

group = "org.islandoftex"
description = "JVM interface to the CTAN JSON API"
version = project.spotlessChangelog.versionNext

repositories {
    mavenCentral()
}

kotlin {
    explicitApi()
    jvm()

    with(sourceSets) {
        commonMain.dependencies {
            implementation(kotlin("stdlib-common"))
            implementation("io.ktor:ktor-client-core:3.0.2")
            implementation("io.ktor:ktor-client-serialization:3.0.2")
            implementation("io.ktor:ktor-client-cio:3.0.2")
            implementation("io.ktor:ktor-client-content-negotiation:3.0.2")
            implementation("io.ktor:ktor-serialization-kotlinx-json:3.0.2")
            api("org.jetbrains.kotlinx:kotlinx-datetime:0.6.1")
        }

        commonTest.dependencies {
            implementation(kotlin("test-common"))
            compileOnly(kotlin("test-annotations-common"))
        }

        jvmTest.dependencies {
            implementation(kotlin("test-junit5"))
            implementation("org.slf4j:slf4j-simple:2.0.16")
            runtimeOnly("org.junit.jupiter:junit-jupiter-engine:5.11.3")
        }
    }

    jvmToolchain(JavaVersionProfile.AS_INTEGER)
}

tasks {
    named<Test>("jvmTest") {
        useJUnitPlatform()

        testLogging {
            events(
                TestLogEvent.FAILED, TestLogEvent.PASSED, TestLogEvent.SKIPPED,
                TestLogEvent.STANDARD_ERROR, TestLogEvent.STANDARD_OUT
            )
        }
    }

    dokkaHtml {
        dokkaSourceSets {
            configureEach {
                jdkVersion.set(JavaVersionProfile.AS_INTEGER)
                moduleName.set("${project.group}.ctanapi")
                includeNonPublic.set(false)
                skipDeprecated.set(false)
                reportUndocumented.set(true)
                skipEmptyPackages.set(true)
                platform.set(org.jetbrains.dokka.Platform.jvm)
                sourceLink {
                    localDirectory.set(file("./"))
                    remoteUrl.set(uri("https://gitlab.com/islandoftex/libraries/ctan-api").toURL())
                    remoteLineSuffix.set("#L")
                }
                noStdlibLink.set(false)
                noJdkLink.set(false)
            }
        }
    }

    withType<DependencyUpdatesTask> {
        rejectVersionIf {
            val stableKeyword = listOf("RELEASE", "FINAL", "GA").any { candidate.version.uppercase() in it }
            val isStable = stableKeyword || "^[0-9,.v-]+$".toRegex().matches(candidate.version)
            isStable.not()
        }
    }
}

spotless {
    kotlinGradle {
        trimTrailingWhitespace()
        endWithNewline()
    }

    kotlin {
        licenseHeader("// SPDX-License-Identifier: BSD-3-Clause")
        target("src/**/*.kt")
        trimTrailingWhitespace()
        endWithNewline()
    }
}

detekt {
    buildUponDefaultConfig = true
    config.setFrom(files("detekt-config.yml"))
}

publishing {
    publications.withType<MavenPublication> {
        pom {
            name.set("Island of TeX CTAN API interface")
            description.set("A JVM library for getting objects out of the CTAN JSON API.")
            inceptionYear.set("2020")
            url.set("https://gitlab.com/islandoftex/libraries/ctan-api")
            organization {
                name.set("Island of TeX")
                url.set("https://gitlab.com/islandoftex")
            }
            licenses {
                license {
                    name.set("BSD 3-clause \"New\" or \"Revised\" License")
                    url.set("https://gitlab.com/islandoftex/libraries/ctan-api/blob/master/LICENSE.md")
                }
            }
            scm {
                connection.set("scm:git:git://gitlab.com/islandoftex/libraries/ctan-api.git")
                developerConnection.set("scm:git:ssh://git@gitlab.com:islandoftex/libraries/ctan-api.git")
                url.set("https://gitlab.com/islandoftex/libraries/ctan-api")
            }
            ciManagement {
                system.set("GitLab")
                url.set("https://gitlab.com/islandoftex/libraries/ctan-api/pipelines")
            }
            issueManagement {
                system.set("GitLab")
                url.set("https://gitlab.com/islandoftex/libraries/ctan-api/issues")
            }
        }
    }

    repositories {
        maven {
            name = "GitLab"
            url = uri("https://gitlab.com/api/v4/projects/20625607/packages/maven")
            credentials(HttpHeaderCredentials::class) {
                if (project.hasProperty("jobToken")) {
                    name = "Job-Token"
                    value = project.property("jobToken").toString()
                }
            }
            authentication {
                create<HttpHeaderAuthentication>("header")
            }
        }
    }
}

object JavaVersionProfile {
    const val AS_INTEGER = 8
    val AS_VERSION = JavaVersion.VERSION_1_8
}
