// SPDX-License-Identifier: BSD-3-Clause
package org.islandoftex.ctanapi

import kotlin.coroutines.CoroutineContext
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.runBlocking

internal actual fun <T> runBlockingNoJs(
    context: CoroutineContext,
    block: suspend CoroutineScope.() -> T
): T =
    runBlocking(context, block)
