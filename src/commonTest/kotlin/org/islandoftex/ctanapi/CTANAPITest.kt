// SPDX-License-Identifier: BSD-3-Clause
package org.islandoftex.ctanapi

import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class CTANAPITest {
    @Test
    fun shouldRetrieveProperPackageDetails() {
        runBlockingNoJs {
            CTANAPI.getDetailsForPackage("arara").let {
                assertEquals("arara", it.name)
                assertEquals("bsd", it.license.first().key)
                assertTrue(it.isInMikTeX)
                assertTrue(it.isInTL)
            }
        }
    }

    @Test
    fun shouldRetrievePackageNamesByTopic() {
        val comparison = listOf("abbrevs", "abbr", "spacingtricks")
        val packages = CTANTopic("abbrev", "").packageNames

        assertTrue(packages.size >= 3)
        comparison.forEach {
            assertTrue(it in packages)
        }
    }

    @Test
    fun shouldRetrievePackagesByTopic() {
        val comparison = listOf("abbrevs", "abbr", "spacingtricks")
        val packages = CTANTopic("abbrev", "").packages
        assertTrue(packages.size >= 3)
        packages.forEach {
            assertTrue(it.maintainer.isNotEmpty())
        }
        val names = packages.map { it.name }
        comparison.forEach {
            assertTrue(it in names)
        }
    }
}
