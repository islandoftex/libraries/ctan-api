// SPDX-License-Identifier: BSD-3-Clause
package org.islandoftex.ctanapi

import kotlin.coroutines.CoroutineContext
import kotlinx.coroutines.CoroutineScope

internal actual fun <T> runBlockingNoJs(
    context: CoroutineContext,
    block: suspend CoroutineScope.() -> T
): T =
    throw IllegalStateException("JS should not call no JS function")
