// SPDX-License-Identifier: BSD-3-Clause
package org.islandoftex.ctanapi

import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.plugins.contentnegotiation.ContentNegotiation
import io.ktor.client.request.get
import io.ktor.serialization.kotlinx.json.json
import kotlin.coroutines.CoroutineContext
import kotlin.coroutines.EmptyCoroutineContext
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.datetime.LocalDate
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json

/**
 * Run a suspension function in a blocking way. Unfortunately, JS does not
 * support blocking execution so this is a workaround.
 */
internal expect fun <T> runBlockingNoJs(
    context: CoroutineContext = EmptyCoroutineContext,
    block: suspend CoroutineScope.() -> T
): T

/**
 * The internal backend for requestion JSON objects from CTAN.
 */
internal object CTANAPI {
    private const val API_VERSION = "2.0"
    private const val CTAN_API_URL = "https://www.ctan.org/json/$API_VERSION"

    private suspend inline fun <reified T> httpRequest(url: String): T =
        HttpClient {
            install(ContentNegotiation) {
                json(Json {
                    isLenient = true
                })
            }
        }.use {
            it.get(url).body()
        }

    // to be queried from /version
    @Serializable
    private data class CTANAPIVersion(val version: String)

    // to read pkgs from /author
    @Serializable
    private data class CTANAuthorPackages(
        val key: String = "",
        val givenname: String = "",
        val von: String = "",
        val familyname: String = "",
        val junior: String = "",
        val pseudonym: String = "",
        val female: Boolean = false,
        val died: Boolean = false,
        @SerialName("pkgs")
        @Serializable(SloppyListSerializer::class)
        val packages: List<String> = listOf()
    )

    // to read packages from /topic
    @Serializable
    private data class CTANTopicPackages(
        val key: String,
        val details: String = "",
        @Serializable(SloppyListSerializer::class)
        val packages: List<String> = listOf()
    )

    // to read packages from /packages
    @Serializable
    private data class CTANPackagesShorthands(
        val key: String,
        val name: String = "",
        val caption: String = ""
    )

    // to read packages from /package
    @Serializable
    private data class CTANDetailedPackageDescription(
        val lang: String? = null,
        val language: String? = null,
        val text: String? = null,
        val description: String? = null
    )

    @Serializable
    private data class CTANDetailedPackageAlias(
        val id: String,
        val name: String
    )

    @Serializable
    private data class CTANDetailedPackageVersion(
        val number: String,
        val date: String?
    )

    @Serializable
    private data class CTANDetailedPackageCTANPath(
        val path: String,
        val file: Boolean = false
    )

    @Serializable
    private data class CTANDetailedPackageDocumentationFile(
        val details: String,
        val language: String?,
        val href: String
    )

    @Serializable
    private data class CTANDetailedPackage(
        val id: String,
        @Serializable(SloppyListSerializer::class)
        val aliases: List<CTANDetailedPackageAlias> = listOf(),
        @Serializable(SloppyListSerializer::class)
        val also: List<String> = listOf(),
        val name: String,
        val caption: String,
        @Serializable(SloppyListSerializer::class)
        val authors: List<CTANPackageMaintainer> = listOf(),
        @Serializable(SloppyListSerializer::class)
        val copyright: List<CTANCopyrightStatement> = listOf(),
        @Serializable(SloppyListSerializer::class)
        val license: List<String> = listOf(),
        val version: CTANDetailedPackageVersion = CTANDetailedPackageVersion("", null),
        @Serializable(SloppyListSerializer::class)
        val documentation: List<CTANDetailedPackageDocumentationFile> = listOf(),
        @Serializable(SloppyListSerializer::class)
        val descriptions: List<CTANDetailedPackageDescription> = listOf(),
        val ctan: CTANDetailedPackageCTANPath? = null,
        val install: String? = null,
        val miktex: String = "",
        val texlive: String = "",
        @Serializable(SloppyListSerializer::class)
        val index: List<String> = listOf(),
        @Serializable(SloppyListSerializer::class)
        val topics: List<String> = listOf(),
        val home: String = "",
        val support: String = "",
        val announce: String = "",
        val bugs: String = "",
        val repository: String = "",
        val development: String = ""
    ) {
        fun toCTANPackage(): CTANPackage = CTANPackage(
            key = id,
            name = name,
            shortDescription = caption,
            longDescriptions = descriptions.associate {
                (it.lang ?: it.language ?: "en") to (it.text ?: it.description ?: "")
            },
            version = CTANVersion(
                version.number,
                if (version.date.isNullOrBlank()) null else LocalDate.parse(version.date)
            ),
            license = license.map {
                CTAN.licenses[it] ?: error("Unknown license used by CTAN for package $id: $it")
            },
            copyright = copyright,
            maintainer = authors,
            sourcesPath = ctan?.path ?: "",
            documentationFiles = documentation.map {
                CTANDocumentationFile(it.details, it.language ?: "en", it.href)
            },
            homePageURL = home,
            supportURL = support,
            bugTrackerURL = bugs,
            repositoryURL = repository,
            announcementURL = announce,
            developmentURL = development,
            isInTL = texlive.isNotEmpty(),
            isInMikTeX = miktex.isNotEmpty(),
            topics = CTAN.topics.filter { it.name in topics },
            seeAlso = aliases.map { it.id }
        )
    }

    /**
     * Check if the API provided at [CTAN_API_URL] conforms to the version
     * specified in the API version we are requesting ([API_VERSION]).
     */
    val isValidVersion: Boolean
        get() = runBlockingNoJs {
            httpRequest<CTANAPIVersion>("$CTAN_API_URL/version")
        }.version == API_VERSION

    /**
     * The licenses CTAN distributes packages with.
     */
    val licenses: List<CTANLicense>
        get() = runBlockingNoJs {
            httpRequest("$CTAN_API_URL/licenses")
        }

    /**
     * Package maintainers and authors using CTAN.
     */
    val authors: List<CTANMaintainer>
        get() = runBlockingNoJs {
            httpRequest("$CTAN_API_URL/authors")
        }

    /**
     * Topics packages are grouped in.
     */
    val topics: List<CTANTopic>
        get() = runBlockingNoJs {
            httpRequest("$CTAN_API_URL/topics")
        }

    /**
     * All packages available on CTAN. Caveat: This makes multiple thousands of requests. Use with care.
     */
    val packages: Flow<CTANPackage>
        get() = flow {
            httpRequest<List<CTANPackagesShorthands>>("$CTAN_API_URL/packages").map {
                emit(getDetailsForPackage(it.key))
            }
        }

    /**
     * Retrieve the package details for package [key].
     */
    suspend fun getDetailsForPackage(key: String): CTANPackage =
        httpRequest<CTANDetailedPackage>("$CTAN_API_URL/pkg/$key").toCTANPackage()

    /**
     * Retrieve the packages by [userName] using the CTAN API.
     *
     * @param userName The identifier of the CTAN user.
     */
    suspend fun getPackagesForMaintainer(userName: String): List<String> =
        httpRequest<CTANAuthorPackages>("$CTAN_API_URL/author/$userName?ref=true").packages

    /**
     * Retrieve the packages of topic [topic] using the CTAN API.
     *
     * @param topic The identifier of the CTAN topic.
     */
    suspend fun getPackagesForTopic(topic: String): List<String> =
        httpRequest<CTANTopicPackages>("$CTAN_API_URL/topic/$topic?ref=true").packages
}
