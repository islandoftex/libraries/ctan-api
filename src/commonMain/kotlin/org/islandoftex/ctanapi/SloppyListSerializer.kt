// SPDX-License-Identifier: BSD-3-Clause
package org.islandoftex.ctanapi

import kotlinx.serialization.KSerializer
import kotlinx.serialization.builtins.ListSerializer
import kotlinx.serialization.json.JsonArray
import kotlinx.serialization.json.JsonElement
import kotlinx.serialization.json.JsonTransformingSerializer

internal class SloppyListSerializer<T>(dataSerializer: KSerializer<T>) :
    JsonTransformingSerializer<List<T>>(ListSerializer(dataSerializer)) {

    // If response is not an array, then it is a single object that should be wrapped into the array
    override fun transformDeserialize(element: JsonElement): JsonElement =
        if (element !is JsonArray) JsonArray(listOf(element)) else element
}
