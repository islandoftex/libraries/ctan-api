// SPDX-License-Identifier: BSD-3-Clause
package org.islandoftex.ctanapi

import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll
import kotlinx.datetime.LocalDate
import kotlinx.serialization.Contextual
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

/**
 * This represents a topic on CTAN. Topics are a way to group packages by their
 * purpose.
 */
@Serializable
public data class CTANTopic(
    /**
     * The identifier of the topic. This identifier is used within the
     * web interface as well.
     */
    @SerialName("key")
    val name: String,
    /**
     * A short description of the topic and its purpose.
     */
    val details: String
) {
    /**
     * The list of packages belonging to the topic. Fetched synchronously from the network.
     */
    public val packageNames: List<String> by lazy {
        runBlockingNoJs {
            CTANAPI.getPackagesForTopic(name)
        }
    }

    /**
     * The list of packages belonging to the topic. Fetched synchronously from the network.
     * Please note that this is the real representation of the full-blown CTAN packages and
     * therefore results in one request per package. Only use when necessary.
     */
    public val packages: List<CTANPackage> by lazy {
        runBlockingNoJs {
            packageNames.map {
                async { CTANAPI.getDetailsForPackage(it) }
            }.awaitAll()
        }
    }
}

/**
 * A maintainer on CTAN is a person or team contributing to a package.
 */
@Serializable
public data class CTANMaintainer(
    /**
     * The user's name is used as the key in the web interface and identifies the
     * user uniquely on CTAN.
     */
    @SerialName("key")
    val userName: String,
    /**
     * The optional title attribute represents the user's (academic) title.
     */
    val title: String = "",
    /**
     * The given name of the user which is optional.
     */
    @SerialName("givenname")
    val givenName: String = "",
    /**
     * The von part of the user's name (optional).
     */
    val von: String = "",
    /**
     * The family name of the user which is optional.
     */
    @SerialName("familyname")
    val familyName: String = "",
    /**
     * The optional name part for numerals, junior and senior attributes.
     */
    val junior: String = "",
    /**
     * The pseudonym of the user. If existent, should be preferred for display over the other things.
     */
    val pseudonym: String = "",
    /**
     * Whether the user is female.
     */
    val female: Boolean = false,
    /**
     * Whether the user already died.
     */
    val died: Boolean = false
) {
    /**
     * The list of packages maintained by this user. Fetched synchronously from the network.
     */
    public val packageNames: List<String> by lazy {
        runBlockingNoJs {
            CTANAPI.getPackagesForMaintainer(userName)
        }
    }

    /**
     * The list of packages maintained by this user. Fetched synchronously from the network.
     * Please note that this is the real representation of the full-blown CTAN packages and
     * therefore results in one request per package. Only use when necessary.
     */
    public val packages: List<CTANPackage> by lazy {
        runBlockingNoJs {
            packageNames.map {
                async { CTANAPI.getDetailsForPackage(it) }
            }.awaitAll()
        }
    }

    override fun toString(): String {
        // override to follow CTAN conventions and print name only if the pseudonym is empty
        return pseudonym.ifEmpty {
            val trimVon = if (von.isNotEmpty()) " $von " else " "
            "$title $givenName$trimVon$familyName $junior".trim()
        }
    }
}

/**
 * A license known to CTAN and used in packages. May include pseudo-licenses.
 */
@Serializable
public data class CTANLicense(
    /**
     * A short identifier for the license.
     */
    val key: String,
    /**
     * The full name of the license.
     */
    val name: String,
    /**
     * Whether CTAN assesses it to be free.
     */
    val free: Boolean
) {
    /**
     * The CTAN url pointing to the license's description or text.
     */
    public val url: String by lazy { "${CTAN.WEBSITE_URL}/license/$key" }
}

/**
 * A CTAN package.
 */
@Serializable
public data class CTANPackage(
    /**
     * The machine-readable key of the package as identifier in the CTAN catalogue.
     */
    val key: String,
    /**
     * The human-readable name of the package as distributed on CTAN.
     */
    val name: String,
    /**
     * A short description (caption) for the package.
     */
    val shortDescription: String,
    /**
     * The long descriptions are sorted by language codes. The default will always be English
     * with language code `en`.
     */
    val longDescriptions: Map<String, String>,
    /**
     * The version of the package. Make sure to understand the documentation of [CTANVersion].
     */
    val version: CTANVersion,
    /**
     * The licenses this package is distributed with.
     */
    val license: List<CTANLicense>,
    /**
     * The list of maintainers and the corresponding timeframe they are maintaining this package.
     */
    val copyright: List<CTANCopyrightStatement>,
    /**
     * The list of current maintainers.
     */
    val maintainer: List<CTANPackageMaintainer>,
    /**
     * The path to the sources on CTAN.
     */
    val sourcesPath: String,
    /**
     * The available documentation files on CTAN.
     */
    val documentationFiles: List<CTANDocumentationFile>,
    /**
     * The package's home page.
     */
    val homePageURL: String,
    /**
     * The package's support page (FAQ or something like that).
     */
    val supportURL: String,
    /**
     * The package's bug tracker.
     */
    val bugTrackerURL: String,
    /**
     * The package's source code repository.
     */
    val repositoryURL: String,
    /**
     * The announcement feed.
     */
    val announcementURL: String,
    /**
     * The URL pointing to the place, package development is taking place at.
     */
    val developmentURL: String,
    /**
     * Whether the package is in TeX Live.
     */
    val isInTL: Boolean,
    /**
     * Whether the package is in MikTeX.
     */
    val isInMikTeX: Boolean,
    /**
     * The topics this package belongs to.
     */
    val topics: List<CTANTopic>,
    /**
     * Aliases of the package.
     */
    val seeAlso: List<String>
)

@Serializable
public data class CTANPackageMaintainer(
    val id: String,
    val active: Boolean
) {
    public fun toCTANMaintainer(): CTANMaintainer = CTAN.authors.first { it.userName == id }
}

/**
 * A CTAN package's copyright statement
 */
@Serializable
public data class CTANCopyrightStatement(
    /**
     * The owner/maintainer of the package in the specified time frame.
     */
    val owner: String,
    /**
     * The time [owner] was responsible for the package.
     */
    @SerialName("year")
    val timeRange: String
)

/**
 * A documentation file as classified by CTAN.
 */
@Serializable
public data class CTANDocumentationFile(
    /**
     * The human readable label.
     */
    val label: String,
    /**
     * An optional language short code.
     */
    val languageCode: String,
    /**
     * The file path of the documentation file.
     */
    val file: String
)

/**
 * The version information of a CTAN package.
 *
 * May be empty string and null for packages like aaai-empty; post-processing required.
 */
@Serializable
public data class CTANVersion(
    /**
     * The version number. May be empty.
     */
    val number: String,
    /**
     * The date of publication.
     */
    @Contextual
    val date: LocalDate?
)
