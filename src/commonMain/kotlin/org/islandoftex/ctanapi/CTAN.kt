// SPDX-License-Identifier: BSD-3-Clause
package org.islandoftex.ctanapi

import kotlinx.coroutines.flow.toList

/**
 * API to fetch objects from CTAN
 */
public object CTAN {
    /**
     * The CTAN web service. Used as base URL for the API routes.
     */
    public const val WEBSITE_URL: String = "https://ctan.org"

    /**
     * The base URL for CTAN mirrors, useful for resolving distributed files.
     */
    public const val MIRROR_NETWORK_URL: String = "https://mirror.ctan.org"

    /**
     * The base URL of CTAN's root mirror.
     */
    public const val ROOT_MIRROR_URL: String = "https://dante.ctan.org/tex-archive"

    /**
     * The licenses CTAN distributes packages with.
     */
    @JvmStatic
    public val licenses: Map<String, CTANLicense>
        get() = CTANAPI.licenses.associateBy { it.key }
            // add the noinfo license used if there is no license information available
            .plus("noinfo" to CTANLicense("noinfo", "No information available", false))

    /**
     * Package maintainers and authors using CTAN.
     */
    @JvmStatic
    public val authors: List<CTANMaintainer>
        get() = CTANAPI.authors

    /**
     * Topics packages are grouped in.
     */
    @JvmStatic
    public val topics: List<CTANTopic>
        get() = CTANAPI.topics

    /**
     * All packages available on CTAN. Caveat: This makes multiple thousands of requests. Use with care.
     */
    @JvmStatic
    public val packages: List<CTANPackage>
        get() = runBlockingNoJs {
            CTANAPI.packages.toList()
        }

    /**
     * Get a detailed object for the given package name.
     *
     * This is provided as fetching the whole [packages] is a tremendous effort for the network
     * and should be avoided.
     */
    @JvmStatic
    public fun getPackageDetails(name: String): CTANPackage =
        runBlockingNoJs {
            CTANAPI.getDetailsForPackage(name)
        }
}
